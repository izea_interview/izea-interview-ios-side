//
//  ViewController.swift
//  Izea_Interview_twitter_client
//
//  Created by DeVaunte Ledee on 2/24/16.
//  Copyright © 2016 DeVaunte Ledee. All rights reserved.
//

import UIKit
import TwitterKit

class ViewController: UIViewController {
    var userId: String!
    var userName: String!
    var userScreenName: String!
    var userImage: NSData?
    var userTokenKey: String!
    var userTokenSecret: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let logInButton = TWTRLogInButton { (session, error) in
            if let unwrappedSession = session {
                let alert = UIAlertController(title: "Logged In",
                    message: "User \(unwrappedSession.userName) has logged in",
                    preferredStyle: UIAlertControllerStyle.Alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                self.presentViewController(alert, animated: true, completion: nil)
                self.loadTwitterAccountDetails()

            } else {
                NSLog("Login error: %@", error!.localizedDescription);
            }
        }

        logInButton.center = self.view.center
        self.view.addSubview(logInButton)

    }
    func loadTwitterAccountDetails(){
        if let session = Twitter.sharedInstance().sessionStore.session() {
            let client = TWTRAPIClient()
            
            client.loadUserWithID(session.userID) { (user, error) -> Void in
                print(session.authToken)
                if let user = user {
                    self.userId = "TwitterUserID:\(user.userID)"
                    print(self.userId!)
                    
                    self.userName = user.name
                    print("TwitterUserName: \(self.userName)")
                    
                    self.userScreenName = user.screenName
                    print("TwitterScreenName: \(self.userScreenName)")
                    
                    let userImageURL = NSURL(string: user.profileImageURL)
                    print("TwitterImageURL: \(userImageURL)")
                    
                    self.userImage = NSData(contentsOfURL: userImageURL!)
//                    let image = UIImage(data: self.userImage!)
                    self.userTokenKey = session.authToken
                    self.userTokenSecret = session.authTokenSecret
                                self.performSegueWithIdentifier("toTimeline", sender: self);

                }

                
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let timelineViewController : TimelineViewController = segue.destinationViewController as! TimelineViewController
            timelineViewController.userToken = self.userTokenKey
        timelineViewController.userTokenSecret = self.userTokenSecret

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


